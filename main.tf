module "simple-app-gke" {
  source		     = "./modules/terraform-google-kubernetes-engine/modules/private-cluster-update-variant/"
  project_id                 = var.project_id
  name                       = var.cluster_name
  region                     = var.region
  regional		     = false
  zones                      = var.zones
  network                    = var.vpc_name
  subnetwork                 = var.subnetwork
  ip_range_pods              = var.secondary_network_b
  ip_range_services          = var.secondary-network_a
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  maintenance_start_time     = "20:00"
  enable_private_nodes       = true
  service_account	     = var.cluster_service_account
  master_ipv4_cidr_block     = var.master_ipv4_cidr_block
  network_policy             = false
  remove_default_node_pool   = true

  node_pools = [
    {
      name               = var.node_pools_name
      machine_type       = var.machine_type
      min_count          = var.min_count
      max_count          = var.max_count
      disk_size_gb       = var.disk_size_gb
      disk_type          = var.disk_type
      image_type         = var.image_type
      auto_repair        = true
      auto_upgrade       = true
      service_account    = var.node_pools_service_account
      preemptible        = var_preemptible
      initial_node_count = 1
    },
  ]

  node_pools_oauth_scopes = {
    all = []

    default-node-pool = [
      "https://www.googleapis.com/auth/cloud-platform",
    ]
  }

  node_pools_labels = {
    all = {}

    default-node-pool = {
      default-node-pool = true
    }
  }

  node_pools_metadata = {
    all = {}

    default-node-pool = {
      node-pool-metadata-custom-value = "simple-app-pools"
    }
  }


  node_pools_tags = {
    all = [
      "all-node",
    ]

    pool-n1-s2 = [
      "simple-app-pools",
    ]
  }
}
