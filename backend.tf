terraform {
  backend "gcs" {
    bucket = "simple-apps-terraform-bucket"
    prefix = "terraform-gke/state"
  }
}

