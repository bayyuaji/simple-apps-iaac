#####################################
#-----control plane's variables-----#
#####################################

variable "project_id" {
  description   = "cluster project id"
  default       = "helloworld"
  type          = string
}

variable "cluster_name" {
  description   = "kubernetes cluster name"
  default       = "helloworld"
  type          = string
}

variable "region" {
  description   = "kubernetes region"
  default       = "asia-southeast2"
}

variable "regional" {
  description   = "kubernetes regional mode"
  default       = false
}

variable "zones" {
  description   = "kubernetes zone"
  default       = ["asia-southeast2-a"]
  type          = list(string)
}

variable "vpc_name" {
  description   = "kubernetes cluster vpc"
  default       = "helloworld"
  type          = string
}

variable "subnetwork" {
  description   = "kubernetes sub network"
  default       = "helloworld"
  type          = string
}

variable "secondary_network_b" {
  description   = "kubernetes secondary network for pods range"
  default       = null
  type          = string
}

variable "secondary_network_a" {
  description   = "kubernetes secondary network for service range"
  default       = null
  type          = string
}

variable "cluster_service_account" {
  description   = "kubernetes service account"
  default       = "gke-sa@famous-dialect-349999.iam.gserviceaccount.com"
  type          = string
}

variable "master_ipv4_cidr_block" {
  description   = "kubernetes control plane block"
  default       = "helloworld
  type          = string
}

####################################
# -----worker node's variables-----#
####################################

variable "node_pools_name" {
  description   = "worker node pools name"
  default       = "helloworld"
  type          = string
}

variable "machine_type" {
  description   = "worker node type machine"
  default       = "n1-standard-2"
  type          = string
}

variable "min_count" {
  description   = "worker node min count"
  default       = 1
}

variable "max_count" {
  description   = "worker node max count"
  default       = 3
}

variable "disk_size_gb" {
  description   = "worker node disk size"
  default       = 60
}

variable "disk_type" {
  description   = "worker node disk type"
  default       = "pd-standard"
  type          = string
}

variable "image_type" {
  description   = "worker node image type"
  default       = "COS"
  type          = string
}

variable "node_pools_service_account" {
  description   = "worker node service account"
  default       = "node-pools-sa@famous-dialect-349999.iam.gserviceaccount.com"
  type          = string
}

variable "preemptible" {
  description   = "worker node preemptible mode"
  default       = false
}
